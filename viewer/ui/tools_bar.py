# -*- coding: utf-8 -*-

__author__ = "LUCE Florentin"

import os
from os import path

from PySide2.QtWidgets import *
from PySide2.QtGui import *
from PySide2.QtCore import *

from viewer.ui.widgets.custom_widgets import *
from viewer.globals import ICONS


class ToolBar(QDockWidget):

    def __init__(self, name, parent):
        super(ToolBar, self).__init__(name, parent)

        self.setTitleBarWidget(QWidget())
        self.setFloating(False)
        self.setAllowedAreas(Qt.LeftDockWidgetArea)
        self.setAllowedAreas(Qt.RightDockWidgetArea)
        parent.addDockWidget(Qt.DockWidgetArea(1), self)

        # Widget docker
        self.backdrop = QWidget(self)
        self.backdrop.setObjectName("docker_backdrop")
        QVBoxLayout(self.backdrop)
        self.backdrop.layout().setContentsMargins(5, 15, 5, 5)
        self.backdrop.layout().setSpacing(3)
        self.backdrop.layout().setAlignment(Qt.AlignTop)
        self.setWidget(self.backdrop)

        self.pb_cursor = ToogleIconButton(f"{ICONS}/tools_bar/cursor.png",
                                      size=(30,30),
                                      layout=self.backdrop.layout())
        self.pb_cursor = ToogleIconButton(f"{ICONS}/tools_bar/draw.png",
                                      size=(30,30),
                                      layout=self.backdrop.layout())
