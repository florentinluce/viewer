# -*- coding: utf-8 -*-

import json
from pathlib import Path

from PySide2.QtCore import Qt, Signal
from PySide2.QtWidgets import QVBoxLayout, QHBoxLayout, QListWidget, \
    QSpacerItem, QFileDialog

from viewer.globals import ICONS
from viewer.objects.stamp import Stamp
from viewer.ui.widgets.custom_widgets import ScrollWidget, PushButton


class StampsTab(ScrollWidget):
    """tab properties with all stamps widgets

    Keyword Arguments:
        layout {Qlayout} -- default: {QVBoxLayout()}
    """

    stamp_updated = Signal(list)

    def __init__(self, layout=QVBoxLayout()):
        super(StampsTab, self).__init__(layout=layout)

        self.layout = layout

        # list stamp layout
        self.stamp_list_layout = QHBoxLayout()
        self.layout.addLayout(self.stamp_list_layout)

        # button list stamp layout
        self.stamp_list_buttons_layout = QVBoxLayout()
        self.stamp_list_buttons_layout.setAlignment(Qt.AlignTop)
        self.stamp_list_buttons_layout.setSpacing(0)

        # list stamp
        self.lw_stamps_list = QListWidget()
        self.lw_stamps_list.setFixedHeight(120)
        self.lw_stamps_list.currentItemChanged.connect(self.get_active_stamp)
        self.stamp_list_layout.addWidget(self.lw_stamps_list)

        # buttons stamp
        self.pb_add_stamp = PushButton(
            "tool_button",
            label="+",
            size=(22, 22),
            position="up",
            layout=self.stamp_list_buttons_layout)
        self.pb_add_stamp.clicked.connect(self.add_stamp)

        self.pb_del_stamp = PushButton(
            "tool_button",
            label="-",
            size=(22, 22),
            position="down",
            layout=self.stamp_list_buttons_layout)
        self.pb_del_stamp.clicked.connect(self.remove_stamp)

        self.stamp_list_buttons_layout.addItem(QSpacerItem(1, 10))

        self.pb_stamp_import = PushButton(
            "tool_button",
            size=(22, 22),
            icon="%s/import_json.png" % ICONS,
            position="up",
            layout=self.stamp_list_buttons_layout)
        self.pb_stamp_import.clicked.connect(self.import_json_stamp)

        self.pb_stamp_export = PushButton(
            "tool_button",
            size=(22, 22),
            icon="%s/export_json.png" % ICONS,
            position="down",
            layout=self.stamp_list_buttons_layout)
        self.pb_stamp_export.clicked.connect(self.export_json_stamp)

        self.stamp_list_layout.addLayout(self.stamp_list_buttons_layout)

    def add_stamp(self, data=None):
        """add a StampItem into stamps list widget

        Keyword Arguments:
            data {dict} -- stamp item data (default: {None})
        """

        if data:
            stamp = Stamp.from_dict(data)
        else:
            stamp = Stamp()

        stamp.lwi_stamp.to_list_widget(self.lw_stamps_list)
        self.layout.addWidget(stamp.gb_stamp)

        self.lw_stamps_list.setFocus()

        stamp.updated.connect(lambda: self.stamp_updated.emit(self.get_stamps_data()))

    def remove_stamp(self):
        """remove selected StampItem and clean the layout"""

        if not len(self.lw_stamps_list.selectedItems()):
            return

        # get current StampItem
        selected_item = self.lw_stamps_list.currentItem()
        selected_item.stamp.remove()

        self.stamp_updated.emit(self.get_stamps_data())

    def import_json_stamp(self):
        """import json file to set stamps"""

        selected_json = QFileDialog.getOpenFileName(
            QFileDialog(),
            "Open json",
            None,
            "json(*.json)")

        json_path = selected_json[0]

        with open(json_path) as file:
            datas = json.load(file)

        for data in datas:
            self.add_stamp(data=data)

        self.stamp_updated.emit(self.get_stamps_data())

    def export_json_stamp(self):
        """export all current stamps to json"""

        export_dir = QFileDialog().getExistingDirectory()
        json_path = Path(export_dir)/"export_stamp.json"

        data = self.get_stamps_data()

        json_path.write_text(json.dumps(data, indent=4))

    def get_active_stamp(self):
        """show layout corresponding to selected StampItem"""

        # close opened StampItem
        for index in range(self.lw_stamps_list.count()):
            stamp = self.lw_stamps_list.item(index).stamp.gb_stamp
            stamp.hide()

        # open selected StampItem
        selected_item = self.lw_stamps_list.currentItem()
        if selected_item:
            selected_item.stamp.gb_stamp.show()

    def get_stamps_data(self):
        """convert all Stamps into dict list

        Returns:
            [list] -- all stamps infos as dict
        """

        data = []
        for index in range(self.lw_stamps_list.count()):
            stamp = self.lw_stamps_list.item(index).stamp
            data.append(stamp.to_dict())

        return data
