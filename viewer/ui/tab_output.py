# -*- coding: utf-8 -*-

from PySide2.QtWidgets import QVBoxLayout, QLabel

from viewer.globals import FILE_TYPE, CODEC, FRAMERATE
from viewer.objects.ffmpeg import Converter
from viewer.objects.codec import Codec
from viewer.lib.utils import convert_type_to_widget, set_joined_widget_position
from viewer.ui.widgets.custom_widgets import ScrollWidget, GroupBox, \
    FormLayout, LineFolder, SpinBox, Slider, EnumList, CheckBox, ComboBox, \
    PushButton


class OutputTab(ScrollWidget):
    """tab properties with output widgets

    Keyword Arguments:
        layout {Qlayout} -- default: {QVBoxLayout()}
    """

    def __init__(self, layout=QVBoxLayout()):
        super(OutputTab, self).__init__(layout=layout)

        # output field
        self.gb_output = GroupBox(
            "Output",
            parent=self.widget,
            layout=FormLayout(),
            margin=(40, 10, 10, 10))

        self.lf_output = LineFolder()
        self.gb_output.layout().addRow("Output", self.lf_output)

        self.cb_stamps = CheckBox("Burn stamps", state=True)
        self.gb_output.layout().addRow("", self.cb_stamps)

        # resolution field
        self.gb_resolution = GroupBox(
            "Dimensions",
            parent=self.widget,
            layout=FormLayout(),
            margin=(40, 10, 10, 10),
            spacing=(0, 10))

        self.sb_width = SpinBox(
            value=1920,
            min=0,
            max=10000,
            unit="px",
            precision=0,
            position="up")
        self.gb_resolution.layout().addRow("Width", self.sb_width)

        self.sb_height = SpinBox(
            value=1080,
            min=0,
            max=10000,
            unit="px",
            precision=0,
            position="middle")
        self.gb_resolution.layout().addRow("Height", self.sb_height)

        self.s_scale = Slider(
            value=50,
            min=0,
            max=100,
            unit="%",
            precision=0,
            position="down")
        self.gb_resolution.layout().addRow("Scale", self.s_scale)

        self.gb_resolution.layout().add_separator(10)

        self.sb_frame_start = SpinBox(
            value=1,
            min=0,
            max=10000,
            precision=0,
            position="up")
        self.gb_resolution.layout().addRow("Frame Start", self.sb_frame_start)

        self.sb_frame_end = SpinBox(
            value=250,
            min=0,
            max=10000,
            precision=0,
            position="middle")
        self.gb_resolution.layout().addRow("End", self.sb_frame_end)

        self.cb_frame_rate = ComboBox(FRAMERATE, position="down")
        self.gb_resolution.layout().addRow("Frame rate", self.cb_frame_rate)

        # codec field
        self.gb_codec = GroupBox(
            "Codec",
            parent=self.widget,
            layout=FormLayout(),
            margin=(40, 10, 10, 10),
            spacing=(0, 10))

        self.el_file_type = EnumList(FILE_TYPE.keys())
        self.el_file_type.button_pressed.connect(self.set_file_containers)
        self.gb_codec.layout().addRow("File type", self.el_file_type)
        self.gb_codec.layout().add_separator(10)

        # watermark field
        self.gb_watermark = GroupBox(
            "Watermark",
            parent=self.widget,
            layout=FormLayout(),
            margin=(40, 10, 10, 10))

        self.cb_watermark = CheckBox("Add watermark")
        self.gb_watermark.layout().addRow("", self.cb_watermark)

        self.l_watermark = QLabel("Path")
        self.lf_watermark = LineFolder()
        self.l_watermark.setEnabled(False)
        self.lf_watermark.setEnabled(False)
        self.gb_watermark.layout().addRow(self.l_watermark, self.lf_watermark)

        self.l_watermark_adjust = QLabel("Adjust")
        self.cb_watermark_adjust = ComboBox(["None", "Fit", "Crop", "Stretch"])
        self.l_watermark_adjust.setEnabled(False)
        self.cb_watermark_adjust.setEnabled(False)
        self.gb_watermark.layout().addRow(self.l_watermark_adjust, self.cb_watermark_adjust)

        self.widget.layout().addStretch(1)

        but = PushButton(label="Export", layout=self.widget.layout())
        but.pressed.connect(self.export_video)

    def set_file_containers(self, button):
        """update layout to set corresponding containers from selected button
        in a combobox

        Arguments:
            button {QAbstractButton} -- current clicked button
        """

        self.file_type = button.text()

        if not hasattr(self, "cb_container"):
            self.cb_container = ComboBox()
            self.cb_container.currentTextChanged.connect(
                self.on_container_changed)
            self.gb_codec.layout().addRow("container", self.cb_container)

        self.cb_container.clear()
        self.cb_container.addItems(FILE_TYPE[self.file_type])

    def on_container_changed(self, container):
        """when current container is changed

        Arguments:
            container {str} -- current selected container
        """

        # update codec layout from selected container
        if container:
            # remove all widgets after cb_container
            self.gb_codec.layout().remove_from_widget(self.cb_container)
            self.set_widget_from_dict(FILE_TYPE[self.file_type][container])

        # change extension in output line edit
        if self.lf_output.value() and self.cb_container.currentText():
            self.change_extension(container)

    def change_extension(self, container):
        """change extension in output path line edit

        Arguments:
            container {str} -- current selected container
        """

        current_path = self.lf_output.value()
        current_ext = current_path.suffix
        ext = FILE_TYPE[self.file_type][container]["extension"]

        if current_ext:
            new_path = current_path.as_posix().replace(current_ext, ext)
            self.lf_output.set_value(new_path)
        else:
            self.lf_output.set_value("%s%s" % (current_path.as_posix(), ext))

    def set_codec_settings(self, codec_name):
        """add widgets for codecs parameters

        Arguments:
            codec {str} -- codec used in output_settings.json as key
        """

        # self.codec = Codec(codec, CODEC[codec])
        # self.set_widget_from_dict(CODEC[codec], self.cb_codec, join=True)

        self.gb_codec.layout().remove_from_widget(self.cb_codec)

        self.codec = Codec(codec_name, CODEC[codec_name])
        all_widgets = self.codec.set_ui()

        # add codec widgets inside layout
        for n, (attr, data) in enumerate(all_widgets.items()):
            self.gb_codec.layout().addRow(attr, data["widget"])

            set_joined_widget_position(data["widget"],
                                       index=n,
                                       max=len(all_widgets.keys())-1)

    def set_widget_from_dict(self, settings_dict, join=False):
        """add widget from dict, key is label and value the widget values

        Arguments:
            settings_dict {dict} -- dict to create and set widget
        Keyword Arguments:
            join {bool} if widgets are joined -- default: {False}
        """

        # set widgets according to the value type in output_settings.json
        for n, (key, value) in enumerate(settings_dict.items()):
            attr_name = key.replace(" ", "_")
            widget = convert_type_to_widget(attr_name, value)
            if not widget:
                continue

            setattr(self, widget[0], widget[1])
            self.gb_codec.layout().addRow(key, widget[1])
            if key == "codec":
                widget[1].currentTextChanged.connect(self.set_codec_settings)

            if join:
                set_joined_widget_position(widget[1],
                                           index=n,
                                           max=len(settings_dict.keys())-1)
            else:
                self.gb_codec.layout().add_separator(10)

    def set_video_data(self, video):
        """change default output values by input video values

        Arguments:
            video {Video} -- opened video
        """

        self.video = video

        self.sb_width.setValue(video.width)
        self.sb_height.setValue(video.height)
        self.cb_frame_rate.setCurrentText(str(video.r_frame_rate))
        self.sb_frame_end.setValue(video.nb_frames)

    def get_output_data(self):
        scale = self.s_scale.value() * 0.01

        output_data = {}
        output_data["output"] = self.lf_output.value().as_posix()
        output_data["width"] = self.sb_width.value() * scale
        output_data["height"] = self.sb_height.value() * scale
        output_data["start_frame"] = self.sb_frame_start.value()
        output_data["end_frame"] = self.sb_frame_end.value()
        output_data["framerate"] = float(self.cb_frame_rate.value())

        if self.el_file_type.checked_value() == "Video":
            output_data["codec"] = self.codec.to_dict()

        output_data["watermark"] = {}
        output_data["watermark"]["path"] = self.lf_watermark.value().as_posix()
        output_data["watermark"]["adjust"] = self.cb_watermark_adjust.value()

        return output_data

    def export_video(self):
        if hasattr(self, "video") and self.lf_output.value():
            print(self.get_output_data())
            stream = Converter.from_dict(self.video, self.get_output_data())
            stream.run()
