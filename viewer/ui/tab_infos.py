# -*- coding: utf-8 -*-

from PySide2.QtWidgets import QVBoxLayout, QLabel, QSizePolicy

from viewer.ui.widgets.custom_widgets import ScrollWidget, GroupBox, \
    FormLayout


class InfosTab(ScrollWidget):
    """tab properties with video informations

    Keyword Arguments:
        layout {Qlayout} -- default: {QVBoxLayout()}
    """

    def __init__(self, layout=QVBoxLayout()):
        super(InfosTab, self).__init__(layout=layout)

    def set_video_data(self, video):
        """append all informations in tab

        Arguments:
            video {[Media} -- media to get all informations
        """

        layout = self.widget.layout()
        for n in reversed(range(layout.count())):
            layout.itemAt(n).widget().setParent(None)

        self.gb_info_formats = GroupBox(
            "Formats",
            parent=self.widget,
            layout=FormLayout(),
            margin=(40, 10, 10, 10))

        for key, value in video.data['format'].items():
            qkey = QLabel(key)
            qvalue = QLabel(str(value))
            qvalue.setWordWrap(True)
            qvalue.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)

            self.gb_info_formats.layout().addRow(qkey, qvalue)

        for n, entry in enumerate(video.data['streams']):
            n += 1
            self.gb_info_stream = GroupBox(
                "Stream %s" % n,
                parent=self.widget,
                layout=FormLayout(),
                margin=(40, 10, 10, 10))

            for key, value in entry.items():
                qkey = QLabel(key)
                qvalue = QLabel(str(value))
                qvalue.setWordWrap(True)
                qvalue.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.MinimumExpanding)

                self.gb_info_stream.layout().addRow(qkey, qvalue)
