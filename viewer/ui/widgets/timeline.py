# -*- coding: utf-8 -*-

"""
    VideoViewer
        Florentin LUCE
    ==============================
    timeline
    custom widgets from timeline
"""

from PySide2.QtCore import Qt, Signal
from PySide2.QtGui import QCursor, QDoubleValidator
from PySide2.QtWidgets import QHBoxLayout, QVBoxLayout, QSlider, QFrame, \
    QLabel, QMenu, QLineEdit, QSizePolicy

from viewer.globals import ICONS, STYLESHEET, FRAMERATE
from viewer.ui.widgets.custom_widgets import IconButton, ToogleIconButton, \
    ComboBox


class TimelineSlider(QSlider):
    """Timeline slider widget

    Arguments:
        frame_start {int} -- video frame start
        frame_end {int} -- video frame end
        framerate {float} -- video frame rate
    """

    def __init__(self, frame_start, frame_end, framerate):
        super(TimelineSlider, self).__init__()

        self.setOrientation(Qt.Horizontal)
        self.setFixedHeight(4)
        self.setMinimum(frame_start)
        self.setMaximum(frame_end)

    def enterEvent(self, event):
        self.setFixedHeight(8)

    def leaveEvent(self, event):
        self.setFixedHeight(4)


class TimeLabel(QLineEdit):
    """Label widget with time display

    Arguments:
        label {string} -- text to display
    Keyword Arguments:
        parent {[type]} -- widget parent (default: {None})
    """

    time_unit_changed = Signal(str)
    changing_frame = Signal()
    change_frame = Signal(int)

    def __init__(self, label, parent=None):
        super(TimeLabel, self).__init__(label)

        self.setProperty("is_integrated", True)
        self.setValidator(QDoubleValidator())
        self.setSizePolicy(QSizePolicy.Maximum, QSizePolicy.Maximum)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.menu)

        if parent:
            parent.layout().addWidget(self)

    def mouseDoubleClickEvent(self, event):
        self.changing_frame.emit()
        self.time_unit_changed.emit("frames")
        self.selectAll()

    def keyPressEvent(self, event):
        super(TimeLabel, self).keyPressEvent(event)
        if event.key() == Qt.Key_Return:
            self.change_frame.emit(int(self.text()))

    def menu(self, pos):
        menu = QMenu()
        menu.addAction("frames", lambda: self.time_unit_changed.emit("frames"))
        menu.addAction("seconds", lambda: self.time_unit_changed.emit("seconds"))
        menu.exec_(QCursor.pos())


class Timeline(QFrame):
    """Timeline object to control video

    Keywords Arguments:
        player_view {PlayerView} -- PlayerView object showing video
    """

    def __init__(self, player_view=None):
        super(Timeline, self).__init__()

        self.player_view = player_view
        self.player = self.player_view.player

        self.video = None
        self.current_frame = 0
        self.time_unit = "seconds"
        self.looping = False

        self.setLayout(QVBoxLayout())
        self.layout().setAlignment(Qt.AlignBottom)
        self.layout().setSpacing(0)
        self.layout().setContentsMargins(0, 0, 0, 0)

        # timeline slider layout
        self.slider_layout = QHBoxLayout()
        self.layout().addLayout(self.slider_layout)

        # control panel
        self.control_panel = QFrame()
        self.control_panel.setObjectName("control_panel")
        QVBoxLayout(self.control_panel)
        self.control_panel.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().addWidget(self.control_panel)

        # control panel layout with timing
        self.time_layout = QHBoxLayout()
        self.time_layout.setAlignment(Qt.AlignCenter)
        self.time_layout.setContentsMargins(2, 0, 2, 0)
        self.control_panel.layout().addLayout(self.time_layout)

        self.l_current_time = TimeLabel("0:00:00")
        self.l_current_time.time_unit_changed.connect(self.player.change_time_unit)
        self.time_layout.addWidget(self.l_current_time)

        self.cb_frame_rate = ComboBox(items=FRAMERATE)
        self.cb_frame_rate.setProperty("is_integrated", True)
        self.cb_frame_rate.setFixedWidth(70)
        self.cb_frame_rate.currentTextChanged.connect(self.player.change_framerate)
        self.time_layout.addWidget(self.cb_frame_rate)
        self.time_layout.addWidget(QLabel("fps"))

        self.time_layout.addStretch()

        self.l_max_time = QLabel("0:00:00")
        self.time_layout.addWidget(self.l_max_time)

        # control panel layout with buttons
        self.control_layout = QHBoxLayout()
        self.control_layout.setAlignment(Qt.AlignCenter)
        self.control_layout.setContentsMargins(10, 10, 10, 20)
        self.control_layout.setSpacing(10)
        self.control_panel.layout().addLayout(self.control_layout)

        self.control_layout.addStretch()

        self.pb_first_frame = IconButton(
            f"{ICONS}/timeline/first_frame.png",
            size=(30, 30),
            shape="circle",
            border=True,
            layout=self.control_layout)
        self.pb_first_frame.clicked.connect(self.player.to_first)

        self.pb_previous_frame = IconButton(
            f"{ICONS}/timeline/previous_frame.png",
            size=(30, 30),
            shape="circle",
            border=True,
            layout=self.control_layout)
        self.pb_previous_frame.clicked.connect(self.player.to_previous)

        self.pb_play = IconButton(
            f"{ICONS}/timeline/play.png",
            size=(35, 35),
            shape="circle",
            border=True,
            layout=self.control_layout)
        self.pb_play.clicked.connect(self.player.toggle_play)

        self.pb_stop = IconButton(
            f"{ICONS}/timeline/stop.png",
            size=(35, 35),
            shape="circle",
            border=True,
            layout=self.control_layout)
        self.pb_stop.clicked.connect(self.player.stop)

        self.pb_next_frame = IconButton(
            f"{ICONS}/timeline/next_frame.png",
            size=(30, 30),
            shape="circle",
            border=True,
            layout=self.control_layout)
        self.pb_next_frame.clicked.connect(self.player.to_next)

        self.pb_last_frame = IconButton(
            f"{ICONS}/timeline/last_frame.png",
            size=(30, 30),
            shape="circle",
            border=True,
            layout=self.control_layout)
        self.pb_last_frame.clicked.connect(self.player.to_last)

        self.control_layout.addStretch()

        self.pb_loop = ToogleIconButton(
            f"{ICONS}/timeline/loop.png",
            size=(20, 20),
            shape="circle",
            border=True,
            layout=self.control_layout)
        self.pb_loop.toggled.connect(self.player.set_looping)

        """self.s_sound = QSlider(Qt.Horizontal)
        self.control_layout.addWidget(self.s_sound)"""

        self.s_timeline = TimelineSlider(0, 100, 25)
        self.player.frame_changed.connect(self.s_timeline.setValue)
        self.player.media_changed.connect(self.set_media_timeline)
        self.player.framerate_changed.connect(
            lambda: self.on_frame_change())
        self.player.played.connect(self.on_player_play)
        self.player.paused.connect(self.on_player_pause)

        self.setStyleSheet(STYLESHEET)

    def set_media_timeline(self, media):

        # clean the older timeline
        for i in reversed(range(self.slider_layout.count())):
            self.slider_layout.itemAt(i).widget().setParent(None)

        self.media = media

        self.s_timeline = TimelineSlider(self.player.start_frame,
                                         self.player.end_frame,
                                         self.player.framerate)
        self.slider_layout.addWidget(self.s_timeline)

        self.cb_frame_rate.setCurrentText(str(self.player.framerate))

        # CONNECTIONS
        self.player.frame_changed.connect(self.on_frame_change)
        self.s_timeline.valueChanged.connect(
            lambda x: setattr(self.player, "current_frame_num", x))

    def on_player_stop(self):

        self.on_player_pause()

        self.s_timeline.setParent(None)
        self.s_timeline = None

        self.video = None

    def on_player_play(self):
        self.pb_play.set_new_icon(f"{ICONS}/timeline/pause.png")

    def on_player_pause(self):
        self.pb_play.set_new_icon(f"{ICONS}/timeline/play.png")

    def on_frame_change(self, current_frame=None):

        if current_frame is not None:
            self.s_timeline.setValue(current_frame)

        if self.player.time_unit == "frames":
            end_frame = str(self.player.end_frame).zfill(4)
            current_frame = str(self.player.current_frame_num).zfill(4)
            self.l_max_time.setText(end_frame)
            self.l_current_time.setText(current_frame)

        elif self.player.time_unit == "seconds":
            self.l_max_time.setText(self.player.end_time)
            self.l_current_time.setText(str(self.player.current_time))

    def is_last_frame(self):
        if self.current_frame == self.s_timeline.maximum():
            return True
        return False
