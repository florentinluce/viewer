# -*- coding: utf-8 -*-

__author__ = "LUCE Florentin"

from pathlib import Path

from PySide2.QtWidgets import QFormLayout, QHBoxLayout, QVBoxLayout, \
    QStackedLayout, QWidget, QFrame, QScrollArea, QSizePolicy, QGroupBox, \
    QPushButton, QFileDialog, QDoubleSpinBox, QAbstractSpinBox, QCheckBox, \
    QColorDialog, QLineEdit, QButtonGroup, QAbstractButton, QComboBox
from PySide2.QtGui import Qt, QIcon, QCursor, QRegion, QImage, QPixmap, \
    QColor, QPainter, QPen, QBrush, QTransform
from PySide2.QtCore import Signal

from viewer.globals import STYLESHEET, ICONS


class FormLayout(QFormLayout):
    """FormLayout widget

    Keyword Arguments:
        parent {QWidget} -- the widget to parent the layout
            (default: {None})
        margin {int or tuple}  -- the margin values (default: {0})
        spacing {int or tuple}  -- the spacing values (default: {0})
        label_alignement {Qt.Alignement} -- the alignement for left element
            (default: {Qt.AlignLeft})
        form_alignement {Qt.Alignement} -- the alignement for right element
            (default: {Qt.AlignLeft})
    """

    def __init__(self, parent=None, margin=0, spacing=(0, 10), label_alignement=Qt.AlignRight, form_alignement=Qt.AlignLeft):
        super(FormLayout, self).__init__(parent)

        self.setLabelAlignment(label_alignement)
        self.setFormAlignment(form_alignement)

        self.setRowWrapPolicy(QFormLayout.DontWrapRows)
        self.setFieldGrowthPolicy(QFormLayout.ExpandingFieldsGrow)

        if isinstance(margin, tuple):
            self.setContentsMargins(margin[0], margin[1], margin[2], margin[3])
        elif isinstance(margin, (float, int)):
            self.setContentsMargins(margin, margin, margin, margin)

        if isinstance(spacing, int):
            self.layout().setSpacing(spacing)
        elif isinstance(spacing, tuple):
            self.layout().setVerticalSpacing(spacing[0])
            self.layout().setHorizontalSpacing(spacing[1])

    def add_separator(self, height):
        """add vertical separator

        Args:
            height (int): separator height in pixels
        """

        separator = QFrame()
        separator.setFixedHeight(height)
        self.addRow(separator, separator)

    def remove_from_widget(self, start_widget):
        """remove all widget in layout from a given one

        Args:
            start_widget (QWidget): [description]
        """

        start = self.getWidgetPosition(start_widget)[0]
        widget_count = self.rowCount()
        for row in reversed(range(start + 1, widget_count)):
            self.removeRow(row)

        self.add_separator(10)


class ScrollWidget(QScrollArea):
    """ScrollArea widget

    Keywords Arguments:
        name {string} -- widget name (default: {None}))
        layout {QLayout} -- layout to set into the scrollArea (default: {None})
    """

    def __init__(self, name=None, layout=None):
        super(ScrollWidget, self).__init__()

        self.setObjectName(name)
        self.setMinimumWidth(250)
        self.setWidgetResizable(True)
        # self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Expanding)

        self.widget = QFrame()
        if layout:
            layout.setAlignment(Qt.AlignTop)
            self.widget.setLayout(layout)
        self.setWidget(self.widget)

        # STYLESHEET
        self.setStyleSheet(STYLESHEET)


class GroupBox(QGroupBox):
    """GroupBox widget with layout control

    Arguments:
        name {string} -- widget name
    Keyword Arguments:
        parent {QWidget} -- widget to parent the groupbox (default: {None})
        layout {QLayout} -- layout to set into the groupbox (default: {None})
        margin {int or tuple} -- the margin values (default: {0})
        spacing {int or tuple}  -- the spacing values (default: {10})
    """

    def __init__(self, name, parent=None, layout=None, margin=0, spacing=10):
        super(GroupBox, self).__init__(name)
        self.setObjectName(name)

        if parent:
            parent.layout().addWidget(self)

        if layout:
            self.setLayout(layout)

        if self.layout():
            if isinstance(margin, tuple):
                self.layout().setContentsMargins(margin[0], margin[1], margin[2], margin[3])
            elif isinstance(margin, (float, int)):
                self.layout().setContentsMargins(margin, margin, margin, margin)

            if isinstance(spacing, int):
                self.layout().setSpacing(spacing)
            elif isinstance(spacing, tuple):
                self.layout().setVerticalSpacing(spacing[0])
                self.layout().setHorizontalSpacing(spacing[1])


class EnumList(QFrame):
    """List widget that select only one item

    Arguments:
        items {list} -- [all items label]
        alignment {string} -- vertical or horizontal (default: {horizontal})
    """

    button_pressed = Signal(QAbstractButton)

    def __init__(self, items, alignment="horizontal"):
        super(EnumList, self).__init__()

        self.setProperty('is_integrated', True)

        # layout
        if alignment == "horizontal":
            self.layout = QHBoxLayout()
        elif alignment == "vertical":
            self.layout = QHBoxLayout()
        self.layout.setSpacing(0),
        self.layout.setContentsMargins(0, 0, 0, 0)
        self.setLayout(self.layout)

        self.group = QButtonGroup()
        self.group.buttonPressed.connect(self.button_pressed.emit)

        # create buttons
        self.buttons_list = []
        for n, item in enumerate(items):
            button = PushButton(label=item, is_checkable=True, layout=self.layout)
            self.group.addButton(button)
            self.buttons_list.append(button)

            if len(items) > 1:
                if n == 0:
                    button.setProperty("position", "left")
                elif n == len(items)-1:
                    button.setProperty("position", "right")
                else:
                    button.setProperty("position", "center")

            self.layout.addWidget(button)

        # STYLESHEET
        self.setStyleSheet(STYLESHEET)

    def checked_button(self):
        return self.group.checkedButton()

    def checked_value(self):
        if self.group.checkedButton():
            return self.checked_button().text()
        else:
            return None


class LineEdit(QLineEdit):
    """LineEdit widget

    Keyword Arguments:
        name {string} -- wigdet name (default: {None})
        text {string} -- text to display in line edit (default: {None})
    """

    def __init__(self, name=None, text=None):
        super(LineEdit, self).__init__(text)
        self.setObjectName(name)

        # STYLESHEET
        self.setStyleSheet(STYLESHEET)

    def mousePressEvent(self, event):
        super(LineEdit, self).mousePressEvent(event)
        self.selectAll()
        self.setFocus()

        # change style sheet
        self.setProperty("is_editable", True)
        self.style().unpolish(self)
        self.style().polish(self)

    def keyPressEvent(self, event):
        super(LineEdit, self).keyPressEvent(event)

    def focusOutEvent(self, event):
        super(LineEdit, self).focusOutEvent(event)
        # change style sheet
        self.setProperty("is_editable", False)
        self.style().unpolish(self)
        self.style().polish(self)

    def value(self):
        return self.text()


class LineFolder(QWidget):
    """LineEdit widget with button to open folder path

    Keyword Arguments:
        name {string} -- widget name (default: {None})
        parent {QWidget} -- widget parent (default: {None})
    """

    def __init__(self, name=None, parent=None):
        super(LineFolder, self).__init__(parent)
        self.setObjectName(name)

        self.layout = QHBoxLayout()
        self.layout.setSpacing(0)
        self.layout.setContentsMargins(0, 0, 0, 0)

        self.line_edit = LineEdit()
        self.line_edit.setProperty("position", "left")
        self.layout.addWidget(self.line_edit)

        self.button = PushButton(size=(20, 20),
                                 icon="%s/widgets/folder.png" % ICONS)
        self.button.setProperty("position", "right")
        self.layout.addWidget(self.button)

        self.setLayout(self.layout)

        self.dir = QFileDialog()

        # STYLESHEET
        self.setStyleSheet(STYLESHEET)

        # CONNECTIONS
        self.button.clicked.connect(self.dir.exec)
        self.dir.currentChanged.connect(self.line_edit.setText)

    def value(self):
        if self.line_edit.value():
            return Path(self.line_edit.value())

    def set_value(self, value):
        self.line_edit.setText(value)


class SpinBox(QDoubleSpinBox):
    """SpinBox widget

    Arguments:
        value {float} -- default value

    Keyword Arguments:
        name {string} -- widget name (default: None)
        min {float} -- minimum value
        max {float} -- maximum value
        unit {str} -- unit after value (default: None)
        precision {int} -- decimals numbers (default: {2})
        parent {QWidget} -- parent of the spinbox (default: {None})
        position {string} -- define styleSheet property from widget position
            up, middle or down (default: {None})
    """

    def __init__(self, value, name=None, min=float('-inf'), max=float('inf'), unit="", precision=2, parent=None, position=None):
        super(SpinBox, self).__init__(parent)
        self.setObjectName(name)
        self.setContextMenuPolicy(Qt.NoContextMenu)

        self._unit = unit
        self._value = value
        self._precision = precision
        self.default_value = value

        self.setRange(min, max)
        self.setValue(value)
        self.setDecimals(precision)

        self.lineEdit().hide()

        # stack layout
        self.layout = QStackedLayout()
        self.layout.setStackingMode(QStackedLayout.StackAll)

        # slider label
        self.label = QLineEdit("%d" % self._value)
        self.label.setEnabled(False)
        self.label.setAlignment(Qt.AlignCenter)
        # self.label.setValidator(QDoubleValidator())
        self.layout.addWidget(self.label)

        self.setLayout(self.layout)

        if position:
            self.setProperty("position", position)

        # STYLESHEET
        self.setStyleSheet(STYLESHEET)

        # CONNECTIONS
        self.valueChanged.connect(self.update)

    def update(self):
        value = self.value()
        if not self._precision:
            value = int(self.value())
        self.label.setText(f"{value} {self._unit}")

    def resizeEvent(self, event):
        self.update()

    def mousePressEvent(self, event):
        super(SpinBox, self).mousePressEvent(event)
        if event.button() == Qt.LeftButton:
            self._x = event.x()
            self._value = self.value()
        if event.button() == Qt.RightButton:
            self.setValue(self.default_value)

    def mouseMoveEvent(self, event):
        self.setCursor(QCursor(Qt.SizeHorCursor))
        offset = event.x() - self._x
        pos = offset * (self.maximum()-self.minimum()) / self.width()

        # reduce values scrolling
        if self.maximum() - self.minimum() > self.width():
            pos = offset

        self.setValue(self._value + pos)

    def mouseDoubleClickEvent(self, event):
        self.label.setEnabled(True)
        self.label.setAlignment(Qt.AlignLeft)
        self.label.selectAll()
        self.label.setFocus()

        # change style sheet
        self.setProperty("editable", True)
        self.style().unpolish(self)
        self.style().polish(self)

    def mouseReleaseEvent(self, event):
        super(SpinBox, self).mouseReleaseEvent(event)
        self.setCursor(QCursor(Qt.ArrowCursor))

    def keyPressEvent(self, event):
        if event.key() != Qt.Key_Return:
            return

        self.setValue(float(self.label.text().replace(self._unit, "")))
        self.label.setAlignment(Qt.AlignCenter)
        self.label.setEnabled(False)

        # change style sheet
        self.setProperty("editable", False)
        self.style().unpolish(self)
        self.style().polish(self)

        # if value dont change, update the mask slider
        self.update()


class Slider(SpinBox):
    """Slider widget

    Arguments:
        value {float} -- default value

    Keyword Arguments:
        name {string} -- widget name (default: None)
        min {float} -- minimul value
        max {float} -- maximum value
        unit {str} -- unit after value (default: None)
        precision {int} -- decimals numbers (default: {2})
        parent {QWidget} -- parent of the slider (default: {None})
        position {string} -- define styleSheet property from widget position
            up, middle or down (default: {None})
    """

    def __init__(self, value, name=None, min=float('-inf'), max=float('inf'), unit="", precision=2, parent=None, position=None):
        super(Slider, self).__init__(value, name=name, min=min, max=max, unit=unit, precision=precision, parent=parent)

        self.setButtonSymbols(QAbstractSpinBox.NoButtons)

        self.slider_frame = QFrame()
        self.slider_frame.setObjectName("slider_fg")
        self.layout.addWidget(self.slider_frame)

        if position:
            self.setProperty("position", position)

        # STYLESHEET
        self.setStyleSheet(STYLESHEET)

    def update(self):
        super(Slider, self).update()
        # set mask on the slider_frame
        mask_width = self.width()*(self.value()-self.minimum())/(self.maximum()-self.minimum())
        if mask_width == 0:
            mask_width = 1
        self.slider_frame.setMask(QRegion(-1, 0, mask_width, self.height()))

    def mousePressEvent(self, event):
        super(Slider, self).mousePressEvent(event)

        if event.button() == Qt.LeftButton:
            self._value = self.minimum() + (self._x * (self.maximum()-self.minimum()) /self.width())
            self.setValue(self._value)

    def mouseDoubleClickEvent(self, event):
        super(Slider, self).mouseDoubleClickEvent(event)
        # hide mask on the slider_frame
        self.slider_frame.setMask(QRegion(-1, 0, 1, self.height()))

    def setProperty(self, prop, value):
        super(Slider, self).setProperty(prop, value)
        self.slider_frame.setProperty(prop, value)


class CheckBox(QCheckBox):
    """CheckBox widget

    Arguments:
        label {string} -- label for the checkbox
    Keyword Arguments:
        name {string} -- widget name (default: {None})
        state {bool} -- default checkbox strate (default: {False})
    """

    def __init__(self, label, name=None, state=False):
        super(CheckBox, self).__init__(label)
        self.setObjectName(name)

        self.setChecked(state)
        self.setLayoutDirection(Qt.RightToLeft)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)

        # STYLESHEET
        self.setStyleSheet(STYLESHEET)


class ComboBox(QComboBox):
    """ComboBox widget

    Arguments:
        items {list} -- all items to add into (default: {""})
        name {string} -- the widget name (default: {None})
        position {string} -- define styleSheet property from widget position
            up, middle or down (default: {None})
    """

    def __init__(self, items="", name=None, position=None):
        super(ComboBox, self).__init__()
        self.setObjectName(name)
        self.setProperty("position", position)

        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)

        self.addItems(items)

    def addItems(self, items):
        self.default = [item for item in items if "$" in item]
        items = [item.replace("$", "") for item in items]

        super(ComboBox, self).addItems(items)

        if self.default:
            self.setCurrentText(self.default[0].replace("$", ""))

    def value(self):
        return self.currentText()


class PushButton(QPushButton):
    """PushButton widget

    Keyword Arguments:
        name {string} -- widget name (default: {None})
        label {string} -- label on button (default: {""})
        size {tuple} -- widget height and width (default: {None})
        icon {string} -- path icon for the widget (default: {None})
        position {string} -- define styleSheet property from widget position
            up, middle or down (default: {None})
        is_checkable {bool} -- if the button can be toogled (default: {False})
        layout {QLayout} -- layout parent (default: {None})
    """

    def __init__(self, name=None, label=None, size=None, icon=None, position=None, is_checkable=False, layout=None):
        super(PushButton, self).__init__(label)
        self.setObjectName(name)
        self.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)

        if size and isinstance(size, tuple):
            self.setFixedSize(size[0], size[1])

        if icon:
            self.icon = Icon(icon)
            self.setIcon(self.icon)
            self.setIconSize(self.size()*0.90)

        if position:
            self.setProperty("position", position)

        if is_checkable:
            self.setCheckable(True)

        if layout:
            layout.addWidget(self)

        # STYLESHEET
        self.setStyleSheet(STYLESHEET)


class ColorButton(PushButton):
    """Button to choose a color

    Arguments:
        color {QColor or tuple} -- background color for button as rgb or
            QColor value
        position {string} -- define styleSheet property from widget position
            up, middle or down (default: {None})
    Keyword Arguments:
        name {[type]} -- widget name (default: {None})
    """

    color_changed = Signal(QColor)

    def __init__(self, color, name=None, position=None):
        super(ColorButton, self).__init__(name=name, position=position)

        if isinstance(color, (tuple, list)):
            color = QColor(color[0]*255.0, color[1]*255.0, color[2]*255.0)

        self.color = color
        self.default_color = color

        self.setObjectName(name)
        self.color_dialog = QColorDialog()

        self.clicked.connect(self.color_dialog.exec)
        self.color_dialog.currentColorChanged.connect(self.set_color)

        self.set_color(color)

    def set_color(self, color):
        self.color = color
        rgb_values = [int(c*255) for c in QColor(self.color).getRgbF()[:-1]]

        self.setStyleSheet(f"""ColorButton{{
            background-color: rgb{tuple(rgb_values)}
            }}""")

        self.color_changed.emit(self.color)

    def mousePressEvent(self, event) :
        super(ColorButton, self).mousePressEvent(event)
        if event.button() == Qt.RightButton:
            self.set_color(self.default_color)


class IconButton(QPushButton):
    """Button with drew icon

    Arguments:
        image_path {[type]} -- icon path
    Keyword Arguments:
        size {tuple} -- widget width and height (default: {None})
        shape {str} -- circle or rect to represent the button's shape
            (default: {"rect"})
        border {bool} -- draw a border around the icon (default: {False})
        layout {Qlayout} -- layout parent (default: {None})
    """

    def __init__(self, image_path, size=None, shape="rect", border=False, layout=None):
        super(IconButton, self).__init__()

        self.setProperty("is_integrated", True)

        self.image_path = image_path
        self.border = border
        self.shape = shape

        if size:
            self.setFixedSize(size[0], size[1])

        self.image = QImage(self.image_path)

        # Icons
        self.icon = self.draw_icon()
        self.icon_pressed = self.draw_icon(bg_color=QColor(120, 182, 201), opacity=1)
        self.icon_hover = self.draw_icon(bg_color=QColor(55, 55, 55))

        # set pixmap onto the label widget
        self.setIcon(self.icon)
        self.setIconSize(self.size())

        if layout:
            layout.addWidget(self)

        # STYLESHEET
        self.setStyleSheet(STYLESHEET)

    def draw_icon(self, bg_color=QColor(Qt.transparent), opacity=0.75):
        image = QImage(self.image)
        p = QPainter(image)

        # clean to delete initial image
        p.setCompositionMode(QPainter.CompositionMode_Clear)
        p.fillRect(image.rect(), QColor(Qt.transparent))
        p.setCompositionMode(QPainter.CompositionMode_SourceOver)

        ratio = self.image.width()/self.width()

        # draw background and border
        if self.border:
            border_pen = QPen(Qt.white)
            border_pen.setWidth(int(1.5*ratio))
            p.setPen(border_pen)

        p.setBrush(QBrush(bg_color))

        # create a shape a bit smaller than the button to avoid cut pixels
        adjusted_border = image.rect().adjusted(ratio, ratio, -ratio, -ratio)
        if self.shape == "rect":
            p.drawRect(adjusted_border)
        elif self.shape == "circle":
            p.drawEllipse(adjusted_border)

        # scale the icon to fit in border
        reduce = int(4*ratio)
        adjusted_image = image.rect().adjusted(reduce, reduce, -reduce, -reduce)
        image_scaled = self.image.scaled(self.size(), Qt.IgnoreAspectRatio, Qt.SmoothTransformation)
        p.drawImage(adjusted_image, image_scaled)

        p.end()

        return Icon(image, opacity=opacity)

    def set_new_icon(self, new_icon_path):

        self.image_path = new_icon_path
        self.image = QImage(self.image_path)
        self.icon = self.draw_icon()
        self.icon_pressed = self.draw_icon(bg_color=QColor(120, 182, 201))
        self.icon_hover = self.draw_icon(bg_color=QColor(55, 55, 55))
        self.setIcon(self.icon)

    def mousePressEvent(self, event):
        super(IconButton, self).mousePressEvent(event)
        self.setIcon(self.icon_pressed)

    def mouseReleaseEvent(self, event):
        super(IconButton, self).mouseReleaseEvent(event)
        self.setIcon(self.icon)

    def enterEvent(self, event):
        super(IconButton, self).enterEvent(event)
        self.setIcon(self.icon_hover)

    def leaveEvent(self, event):
        super(IconButton, self).leaveEvent(event)
        self.setIcon(self.icon)


class ToogleIconButton(IconButton):
    """Button with drew icon

    Arguments:
        image_path {[type]} -- icon path
    Keyword Arguments:
        state {Bool} -- starting button state (default: {False})
        size {tuple} -- widget width and height (default: {None})
        shape {str} -- circle or rect to represent the button's shape
            (default: {"rect"})
        border {bool} -- draw a border around the icon (default: {False})
        layout {Qlayout} -- layout parent (default: {None})
    """

    def __init__(self, image_path, state=False, size=None, shape="rect", border=False, layout=None):
        super(ToogleIconButton, self).__init__(image_path, size=size, shape=shape, border=border, layout=layout)

        self.setCheckable(True)
        self.setChecked(state)
        self.is_active = self.isChecked()
        self.toggled.connect(self.change_icons)

        self.icon_off = self.draw_icon()
        self.icon_off_hover = self.draw_icon(bg_color=QColor(55, 55, 55))
        self.icon_on = self.draw_icon(bg_color=QColor(120, 182, 201), opacity=1)

    def change_icons(self):
        if self.isChecked():
            # turn on
            self.icon = self.icon_on
            self.icon_hover = self.icon_on
            self.is_active = True

        else:
            # turn off
            self.icon = self.icon_off
            self.icon_hover = self.icon_off_hover
            self.is_active = False


class Icon(QIcon):
    """Icon widget

    Arguments:
        icon {str, QImage or QPixmap} -- the icon path or image widget
    Keyword Arguments:
        opacity {float} -- value between 0 and 0.1 to set icon opacity
            (default: {0.75})
        rotate {int} -- value to rotate the icon (default: {0})
    """

    def __init__(self, icon, opacity=0.75, rotate=0):
        super(Icon, self).__init__()

        if isinstance(icon, str):
            self.image = QImage(icon)
        elif isinstance(icon, QPixmap):
            self.image = icon.toImage()
        elif isinstance(icon, QImage):
            self.image = icon

        if rotate:
            trans = QTransform()
            trans.rotate(rotate)
            self.image = self.image.transformed(trans)

        p = QPainter(self.image)
        p.setCompositionMode(QPainter.CompositionMode_DestinationIn)
        p.fillRect(self.image.rect(), QColor(0, 0, 0, opacity*255))
        p.end()

        self.addPixmap(QPixmap.fromImage(self.image))


if __name__ == "__main__":
    import sys
    from PySide2.QtWidgets import QApplication

    path = "/Users/florentin/Documents/Scripts/viewer"
    if path not in sys.path:
        sys.path.append(path)

    from viewer.globals import ICONS, STYLESHEET

    app = QApplication(sys.argv)

    ui = QWidget()
    ui.setLayout(QVBoxLayout())

    enum = EnumList()

    ui.layout().addWidget(enum)

    ui.show()
    sys.exit(app.exec_())
