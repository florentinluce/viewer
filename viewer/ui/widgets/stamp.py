__author__ = "LUCE Florentin"

from functools import partial

from PySide2.QtCore import Qt, Signal, QObject
from PySide2.QtGui import QPixmap, QCursor
from PySide2.QtWidgets import QMenu, QListWidgetItem, QGroupBox, QLabel, \
    QComboBox, QSizePolicy, QHBoxLayout, QLineEdit, QWidget

from viewer.globals import ICONS, STAMP_INFOS
from viewer.ui.widgets.custom_widgets import CheckBox, LineEdit, ColorButton, \
    SpinBox, Slider, FormLayout


class StampGroupBox(QGroupBox):
    """Stamp object with all tag information

    Keyword Arguments:
        name {string} -- stap name (default: {None})
    """

    data_updated = Signal()

    def __init__(self, stamp):
        super(StampGroupBox, self).__init__()

        self.stamp = stamp
        self.set_ui()

    def on_data_updated(self):
        """convert main attributes found in stamp_infos['main_attributs']
        into dictionnary

        Returns:
            [dict] -- stamp dictionnary
        """

        # get all values
        self.stamp.text = self.le_text.text()
        self.stamp.position = self.cb_position.currentText()
        self.stamp.on_video = self.cb_is_inside.isChecked()
        self.stamp.text_size = self.sb_text_size.value()
        self.stamp.text_color = self.pb_text_color.color.getRgbF()[:-1]
        self.stamp.text_alpha = self.s_text_alpha.value()
        self.stamp.bg_color = self.pb_bg_color.color.getRgbF()[:-1]
        self.stamp.bg_alpha = self.s_bg_alpha.value()

        self.stamp.updated.emit()

    def set_ui(self):
        """create the ui workspace to set the main attributes

        Returns:
            [QGroupBox] -- workspace widget
        """

        self.setSizePolicy(QSizePolicy.Preferred, QSizePolicy.Maximum)
        self.setTitle(self.stamp.name)

        FormLayout(self, margin=(40, 10, 10, 10), spacing=10)

        # stamp text
        self.le_text = StampLineEdit('stamp_text', text=self.stamp.text)
        self.le_text.textChanged.connect(self.on_data_updated)
        self.layout().addRow("Text", self.le_text)

        # stamp size
        self.sb_text_size = SpinBox(
            self.stamp.text_size,
            min=5,
            max=100,
            precision=0)
        self.sb_text_size.valueChanged.connect(self.on_data_updated)
        self.layout().addRow("Size", self.sb_text_size)

        # stamp color
        self.pb_text_color = ColorButton(self.stamp.text_color, position="left")
        self.pb_text_color.color_changed.connect(self.on_data_updated)
        self.s_text_alpha = Slider(
            self.stamp.text_alpha,
            min=0,
            max=1,
            position="right")
        self.s_text_alpha.valueChanged.connect(self.on_data_updated)
        lay = QHBoxLayout()
        lay.setSpacing(0)
        lay.addWidget(self.pb_text_color)
        lay.addWidget(self.s_text_alpha)
        self.layout().addRow("Color", lay)

        self.pb_bg_color = ColorButton(self.stamp.bg_color, position="left")
        self.pb_bg_color.color_changed.connect(self.on_data_updated)
        self.s_bg_alpha = Slider(
            value=self.stamp.bg_alpha,
            min=0,
            max=1,
            position="right")
        self.s_bg_alpha.valueChanged.connect(self.on_data_updated)
        lay = QHBoxLayout()
        lay.setSpacing(0)
        lay.addWidget(self.pb_bg_color)
        lay.addWidget(self.s_bg_alpha)
        self.layout().addRow("Bar", lay)

        # stamp position
        self.cb_position = QComboBox()
        self.cb_position.setSizePolicy(QSizePolicy.Expanding, QSizePolicy.Preferred)
        self.cb_position.addItems([i for i in self.stamp.text_positions])
        self.cb_position.setCurrentText(self.stamp.position)
        self.cb_position.currentTextChanged.connect(self.on_data_updated)
        self.layout().addRow("Position", self.cb_position)

        # stamp on video
        self.cb_is_inside = CheckBox("On video", state=self.stamp.on_video)
        self.cb_is_inside.stateChanged.connect(self.on_data_updated)
        self.layout().addRow("", self.cb_is_inside)


class StampItem(QListWidgetItem):
    """List widget item that carry the stamp object

    Arguments:
        parent {QListWidget} -- the list widget to set item in
    Keyword Arguments:
        data {dict} -- dict with all informations found
        in stamp_infos['main_attributs'] (default: {None})
    """

    def __init__(self, stamp):
        super(StampItem, self).__init__()

        self.stamp = stamp

        self.widget = QWidget()
        self.widget.setProperty("is_integrated", True)
        QHBoxLayout(self.widget)
        self.widget.layout().setContentsMargins(5, 0, 10, 0)

        self.icon = QLabel()
        pixmap = QPixmap("%s/stamp.png" % ICONS)
        self.icon.setPixmap(pixmap.scaled(18, 18))
        self.widget.layout().addWidget(self.icon)

        self.le_name = QLineEdit(self.stamp.name)
        self.le_name.setProperty("is_integrated", True)
        self.le_name.textChanged.connect(lambda x: self.stamp.set_name(x))
        self.widget.layout().addWidget(self.le_name)

        self.setSizeHint(self.widget.sizeHint())

    def to_list_widget(self, list_widget):
        list_widget.addItem(self)
        list_widget.setItemWidget(self, self.widget)
        list_widget.setCurrentItem(self)

    def remove(self):
        list_widget = self.listWidget()
        index = list_widget.row(self)
        list_widget.takeItem(index)


class StampLineEdit(LineEdit):
    """Custom widget for stamp line edit

    Arguments:
        name {string} -- widget name
        text {string} -- text to display inside
    """

    def __init__(self, name, text):
        super(StampLineEdit, self).__init__(name, text=text)

        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.menu)

    def menu(self, pos):
        """Create a right click menu to set stamp expression"""

        menu = QMenu()
        for label, value in STAMP_INFOS["expression"].items():
            menu.addAction(label, partial(self.setText, "%s{%s}" % (self.text(), value)))
        menu.exec_(QCursor.pos())
