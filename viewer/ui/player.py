# -*- coding: utf-8 -*-

__author__ = "LUCE Florentin"

import re

from PySide2.QtWidgets import QVBoxLayout, QHBoxLayout, QWidget, QFrame, \
    QLabel, QFileDialog, QStackedLayout
from PySide2.QtGui import QColor, QImage, QPixmap, QRegion
from PySide2.QtCore import Signal, Qt, QPoint, QRect

from viewer.ui.widgets.timeline import Timeline


class ImageContainer(QLabel):
    """Label to draw each video frame and stamps"""

    color_picked = Signal(QColor, QPoint)

    def __init__(self):
        super(ImageContainer, self).__init__()

        self.setScaledContents(True)
        self.setAlignment(Qt.AlignCenter)

        # main layout
        QVBoxLayout(self)
        self.layout().setContentsMargins(0, 0, 0, 0)

        # top and bot stamp layout
        self.top_stamp = StampFrame(Qt.AlignTop, self)
        self.layout().addStretch()
        self.bottom_stamp = StampFrame(Qt.AlignBottom, self)

    def mouseMoveEvent(self, event):
        if not self.pixmap():
            return

        x = event.pos().x()*self.pixmap().width()/self.width()
        y = event.pos().y()*self.pixmap().height()/self.height()

        # keep only pixel on pixmap
        if (x <= 0 or x >= self.pixmap().width() or
           y <= 0 or y >= self.pixmap().height()):
            return

        rgb = self.pixmap().toImage().pixel(x, y)
        self.color_picked.emit(QColor(rgb), QPoint(x, y))

    def mousePressEvent(self, event):
        if not self.pixmap():
            QFileDialog.getExistingDirectory(None,
                                             "Select a folder:",
                                             "",
                                             QFileDialog.ShowDirsOnly)


class StampFrame(QFrame):
    """Video Stamp widget to add on video

    Arguments:
        alignment {Qt.Alignement} -- alignement of the label
        parent {QWidget} -- parent widget
    """

    def __init__(self, alignment, parent):
        super(StampFrame, self).__init__()

        self.setObjectName("tag_bg")
        parent.layout().addWidget(self)

        QHBoxLayout(self)
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setAlignment(alignment)

        # set children layouts
        self.left_layout = QVBoxLayout()
        self.left_layout.setSpacing(0)
        self.layout().addLayout(self.left_layout)

        self.layout().addStretch()

        self.center_layout = QVBoxLayout()
        self.center_layout.setSpacing(0)
        self.layout().addLayout(self.center_layout)

        self.layout().addStretch()

        self.right_layout = QVBoxLayout()
        self.right_layout.setSpacing(0)
        self.layout().addLayout(self.right_layout)


class PlayerView(QWidget):
    """Video player view to display frames"""

    frame_changed = Signal(int)

    def __init__(self, player):
        super(PlayerView, self).__init__()

        self.player = player
        self.used_stamps = []

        self.setAcceptDrops(True)

        # Create a layout
        QStackedLayout(self)
        self.layout().setStackingMode(QStackedLayout.StackAll)
        self.layout().setContentsMargins(0, 0, 0, 0)
        self.layout().setSpacing(0)

        # Timeline
        self.timeline = Timeline(player_view=self)
        self.layout().addWidget(self.timeline)

        # view widget
        self.w_view = QWidget()
        self.w_view.setStyleSheet("QWidget{background:black}")
        QVBoxLayout(self.w_view)
        self.w_view.layout().setContentsMargins(0, 0, 0, self.timeline.control_panel.height())
        self.w_view.layout().setSpacing(0)
        self.layout().insertWidget(0, self.w_view)
        self.layout().setAlignment(self.w_view, Qt.AlignBottom)

        # Top stamp outside video
        self.w_view.layout().addStretch()
        self.w_view.top_stamp = StampFrame(Qt.AlignBottom, self.w_view)

        # View image label
        self.l_image = ImageContainer()
        self.l_image.setText("Drop your video")
        self.w_view.layout().addWidget(self.l_image)

        # Top stamp inside video
        self.w_view.bottom_stamp = StampFrame(Qt.AlignTop, self.w_view)
        self.w_view.layout().addStretch()

        # Create a mask on player stacked layout to reveal view layer
        self.view_mask = self.update_mask()

        self.player.stamps_updated.connect(self.set_stamps)

    def dragEnterEvent(self, event):
        if event.mimeData().hasUrls():
            event.accept()

    def dropEvent(self, event):
        event_path = [url.toLocalFile() for url in event.mimeData().urls()][0]

        self.player.set_media(event_path)

    def resizeEvent(self, event):
        ratio = 0.56
        if self.player.media:
            ratio = self.player.media.ratio

        self.l_image.setFixedHeight(self.width()/ratio)
        self.update_mask()

    def update_mask(self):
        mask_height = self.w_view.height()-self.timeline.control_panel.height()
        self.view_mask = QRect(0,
                               mask_height - 10,
                               self.timeline.control_panel.width(),
                               self.timeline.control_panel.height() + 10)

        self.timeline.clearMask()
        self.timeline.setMask(QRegion(self.view_mask, QRegion.Rectangle))
        return self.view_mask

    def set_frame_in_player(self, frame):

        image = QImage(frame, self.player.media.width, self.player.media.height, QImage.Format_RGB888)
        self.pixmap = QPixmap.fromImage(image)
        self.l_image.setPixmap(self.pixmap)
        self.l_image.setFixedHeight(self.width()/self.player.media.ratio)

    def close_video(self):
        self.player.media = None
        self.l_image.setText("Drop your video")

    def set_background_color(self, color):
        rgb_values = QColor(color).getRgbF()

        self.w_view.setStyleSheet("""QWidget{
            background-color: rgb(%s,%s,%s)
            }""" % (rgb_values[0]*255,
                    rgb_values[1]*255,
                    rgb_values[2]*255))

    def set_stamps(self):

        # delete all stamps to create new ones
        for label in self.used_stamps:
            label.setParent(None)

        for stamp in self.player.stamps:
            if not stamp.text:
                continue

            # set the tag background with position
            stamp_pos_y = stamp.position.split('-')[0]
            stamp_pos_x = stamp.position.split('-')[1]
            if stamp.on_video:
                stamp_frame = getattr(self.l_image, f"{stamp_pos_y}_stamp")
            else:
                stamp_frame = getattr(self.w_view, f"{stamp_pos_y}_stamp")
            layout = getattr(stamp_frame, f"{stamp_pos_x}_layout")

            color = [int(c*255) for c in stamp.bg_color]
            alpha = int(stamp.bg_alpha*255)
            stamp_frame.setStyleSheet(f"""#tag_bg{{
                background-color:rgba{tuple(color + [alpha])};
                }}""")

            text = str(stamp.text)
            expressions = re.findall(r"{(.*?)}", text)
            for exp in expressions:
                value = eval("self."+exp)
                text = text.replace("{%s}" % exp, str(value))

            l_stamp = QLabel(text)
            l_stamp.setObjectName("tag")
            layout.addWidget(l_stamp)

            color = [int(c*255) for c in stamp.text_color]
            alpha = int(stamp.text_alpha*255)
            l_stamp.setStyleSheet(f"""#tag{{
                color:rgba{tuple(color + [alpha])};
                font-size:{int(stamp.text_size)}px;
                background-color:transparent
                }}""")

            # update the stamp dict and label
            self.used_stamps.append(l_stamp)
