# -*- coding: utf-8 -*-

"""
    VideoViewer
        Florentin LUCE
    ==============================
    properties-bar
    Docker with the properties bar
"""

from PySide2.QtCore import Qt, QSize
from PySide2.QtWidgets import QWidget, QDockWidget, QTabWidget, QSizePolicy

from viewer.globals import ICONS
from viewer.ui.widgets.custom_widgets import Icon
from viewer.ui.tab_output import OutputTab
from viewer.ui.tab_stamps import StampsTab
from viewer.ui.tab_display import DisplayTab
from viewer.ui.tab_infos import InfosTab


class PropertiesBar(QDockWidget):

    def __init__(self, name, parent):
        super(PropertiesBar, self).__init__(name, parent)

        # Docker
        self.setFloating(False)
        self.setAllowedAreas(Qt.RightDockWidgetArea)
        self.setContentsMargins(0, 0, 0, 0)
        self.setTitleBarWidget(QWidget())
        parent.addDockWidget(Qt.DockWidgetArea(2), self)

        # Tab widget
        self.tab = QTabWidget(self)
        self.tab.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        self.tab.setObjectName("tab")
        self.tab.setTabPosition(QTabWidget.West)
        self.setWidget(self.tab)

        self.set_output_tab(0, "%s/output_tab.png" % ICONS)
        self.set_stamp_tab(1, "%s/stamp_tab.png" % ICONS)
        self.set_display_tab(2, "%s/display_tab.png" % ICONS)
        self.set_informations_tab(3, "%s/info_tab.png" % ICONS)
        self.tab.setIconSize(QSize(24, 24))

    def set_output_tab(self, index, icon):
        """Initialize the tab for video output settings

        Arguments:
            index {int} -- the position of the tab (start at 0)
            icon {string} -- the tab's icon path
        """

        self.output_tab = OutputTab()
        self.tab.addTab(self.output_tab, "")
        self.tab.setTabToolTip(index, "Output settings")
        self.tab.setTabIcon(index, Icon(icon, rotate=90))

    def set_stamp_tab(self, index, icon):
        """Initialize the tab for video stamp and text

        Arguments:
            index {int} -- the position of the tab (start at 0)
            icon {string} -- the tab's icon path
        """

        self.stamps_tab = StampsTab()
        self.tab.addTab(self.stamps_tab, "")
        self.tab.setTabToolTip(index, "Stamp settings")
        self.tab.setTabIcon(index, Icon(icon, rotate=90))

    def set_display_tab(self, index, icon):
        """Initialize the tab for pixel and display informations

        Arguments:
            index {int} -- the position of the tab (start at 0)
            icon {string} -- the tab's icon path
        """

        self.display_tab = DisplayTab()
        self.tab.addTab(self.display_tab, "")
        self.tab.setTabToolTip(index, "Display and pixels")
        self.tab.setTabIcon(index, Icon(icon, rotate=90))

    def set_informations_tab(self, index, icon):
        """Initialize the tab for video informations get from ffprobe

        Arguments:
            index {int} -- the position of the tab (start at 0)
            icon {string} -- the tab's icon path
        """

        self.infos_tab = InfosTab()
        self.tab.addTab(self.infos_tab, "")
        self.tab.setTabToolTip(index, "Video informations")
        self.tab.setTabIcon(index, Icon(icon, rotate=90))
