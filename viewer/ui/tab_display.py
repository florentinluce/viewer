# -*- coding: utf-8 -*-

from PySide2.QtCore import Signal, Qt
from PySide2.QtGui import QColor
from PySide2.QtWidgets import QVBoxLayout, QHBoxLayout, QFrame, QSizePolicy, \
    QWidget, QLabel

from viewer.ui.widgets.custom_widgets import ScrollWidget, GroupBox, \
    FormLayout, Slider, ColorButton


class DisplayTab(ScrollWidget):
    """tab properties with pixels informations

    Keyword Arguments:
        layout {Qlayout} -- default: {QVBoxLayout()}
    """

    gamma_changed = Signal(float)
    exposure_changed = Signal(float)

    def __init__(self, layout=QVBoxLayout()):
        super(DisplayTab, self).__init__(layout=layout)

        # pixels info
        self.gb_pixel = GroupBox(
            "Pixel informations",
            parent=self.widget,
            layout=QHBoxLayout(),
            margin=(40, 10, 10, 10))

        self.f_pixcolor = QFrame()
        self.f_pixcolor.setMaximumSize(70, 70)
        self.f_pixcolor.setSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        self.f_pixcolor.setAutoFillBackground(True)
        QHBoxLayout(self.f_pixcolor)
        self.f_pixcolor.layout().addStretch(1)
        self.gb_pixel.layout().addWidget(self.f_pixcolor)

        self.w_pixinfo = QWidget()
        self.w_pixinfo.setProperty("is_integrated", True)
        self.w_pixinfo.setStyleSheet("QLabel{font-size: 10px}")
        FormLayout(self.w_pixinfo)
        self.l_pixinfo_r = QLabel('')
        self.w_pixinfo.layout().addRow('Red', self.l_pixinfo_r)
        self.l_pixinfo_g = QLabel('')
        self.w_pixinfo.layout().addRow('Green', self.l_pixinfo_g)
        self.l_pixinfo_b = QLabel('')
        self.w_pixinfo.layout().addRow('Blue', self.l_pixinfo_b)
        self.l_pixinfo_a = QLabel('')
        self.w_pixinfo.layout().addRow('Alpha', self.l_pixinfo_a)
        self.l_pixinfo_pos = QLabel('')
        self.w_pixinfo.layout().addRow('XY', self.l_pixinfo_pos)
        self.gb_pixel.layout().addWidget(self.w_pixinfo)

        # color info
        self.gb_color_management = GroupBox(
            "Color management",
            parent=self.widget,
            layout=FormLayout(),
            margin=(40, 10, 10, 10),
            spacing=(0, 10))

        self.s_exposure = Slider(
            value=0,
            min=-3,
            max=3,
            precision=3,
            position="up")
        self.s_exposure.valueChanged.connect(lambda x: self.exposure_changed.emit(x))
        self.gb_color_management.layout().addRow("Exposure", self.s_exposure)

        self.s_gamma = Slider(
            value=1,
            min=0,
            max=5,
            precision=1,
            position="down")
        self.s_gamma.valueChanged.connect(lambda x: self.gamma_changed.emit(x))
        self.gb_color_management.layout().addRow("Gamma", self.s_gamma)

        # background
        self.gb_background = GroupBox(
            "Background",
            parent=self.widget,
            layout=FormLayout(),
            margin=(40, 10, 10, 10))

        self.pb_bg_color = ColorButton(Qt.black)
        self.gb_background.layout().addRow("Color", self.pb_bg_color)

    def set_pixel_values(self, color, pos):
        """[summary]

        Arguments:
            color {QColor} -- Color to display as rgb
            color {QPoint} -- Mouse position to display
        """

        rgb_values = QColor(color).getRgbF()

        # show rgb values
        self.l_pixinfo_r.setText("%s" % str(rgb_values[0])[:9])
        self.l_pixinfo_g.setText("%s" % str(rgb_values[1])[:9])
        self.l_pixinfo_b.setText("%s" % str(rgb_values[2])[:9])
        self.l_pixinfo_a.setText("%s" % str(rgb_values[3])[:9])
        self.l_pixinfo_pos.setText("%s, %s" % (pos.x(), pos.y()))

        # change color
        self.f_pixcolor.setStyleSheet("""QFrame{
            background-color: rgb(%s,%s,%s)
            }""" % (rgb_values[0]*255,
                    rgb_values[1]*255,
                    rgb_values[2]*255))
