# -*- coding: utf-8 -*-

"""
    VideoViewer
        Florentin LUCE
    ==============================
    main_window
    Create the main interface
"""

from PySide2.QtWidgets import QMainWindow

from viewer.objects.player import Player

from viewer.globals import STYLESHEET #, APP
from viewer.ui.player import PlayerView
from viewer.ui.tools_bar import ToolBar
from viewer.ui.properties_bar import PropertiesBar


class Window(QMainWindow):
    """The main window"""

    def __init__(self):
        super(Window, self).__init__()

        self.set_ui()

        #APP.player = self.player


        self.show()

        # CONNECTIONS
        # enable widget on checkbox
        self.properties_bar.output_tab.cb_watermark.stateChanged.connect(
            lambda: self.enable_widget(
                    self.properties_bar.output_tab.cb_watermark, [
                        self.properties_bar.output_tab.l_watermark,
                        self.properties_bar.output_tab.lf_watermark,
                        self.properties_bar.output_tab.l_watermark_adjust,
                        self.properties_bar.output_tab.cb_watermark_adjust]))

        # set informations when video is dropped
        self.player.media_changed.connect(
            self.properties_bar.infos_tab.set_video_data)
        self.player.media_changed.connect(
            self.properties_bar.output_tab.set_video_data)

        # connect framerate
        self.central_widget.timeline.cb_frame_rate.currentTextChanged.connect(
            self.properties_bar.output_tab.cb_frame_rate.setCurrentText)

        # show color picked on frame
        self.central_widget.l_image.color_picked.connect(
            self.properties_bar.display_tab.set_pixel_values)

        # change gamma and exposure
        self.properties_bar.display_tab.gamma_changed.connect(
            self.player.adjust_gamma)
        self.properties_bar.display_tab.exposure_changed.connect(
            self.player.adjust_exposure)

        # set background color
        self.properties_bar.display_tab.pb_bg_color.color_changed.connect(
            self.central_widget.set_background_color)

        # set stamps
        """self.properties_bar.stamps_tab.pb_add_stamp.clicked.connect(
            self.player.add_stamp())
        self.properties_bar.stamps_tab.pb_del_stamp.clicked.connect(
            self.player.remove_stamp())"""

        self.properties_bar.stamps_tab.stamp_updated.connect(
            self.player.set_stamps)
        """self.central_widget.frame_changed.connect(
            lambda: self.central_widget.set_stamps(
                    self.properties_bar.stamps_tab.get_stamps_data()))"""

    def set_ui(self):
        """Initialize all the docker and widget in the window"""

        self.setWindowTitle("Player[*]")
        self.resize(1024, 600)
        self.setMinimumSize(1024, 512)

        self.tools_bar = ToolBar("Tools bar", self)

        self.properties_bar = PropertiesBar("Properties", self)

        self.player = Player()
        self.central_widget = self.player.player_view
        self.setCentralWidget(self.central_widget)

        # STYLESHEET
        self.setStyleSheet(STYLESHEET)

    def enable_widget(self, check_box, targets_list):
        """Enable multiple QWidgets by a driver checkbox

        Arguments:
            check_box {QCheckbox} -- a QCheckbox widget or a inherit one
            targets_list {QWidget} -- a list of all QWidgets which have to be
                enabled or not
        """

        for target in targets_list:
            if check_box.isChecked():
                target.setEnabled(True)
            elif not check_box.isChecked():
                target.setEnabled(False)
