import json
from pathlib import Path
#from PySide2.QtCore import QApplication

ROOT = Path(__file__).parent

RESOURCES = ROOT/'ui'/'ressources'
ICONS = RESOURCES/'icons'
STYLESHEET = (RESOURCES/'stylesheet.css').read_text()

STAMP_INFOS = json.loads((ROOT/'lib'/'stamp_infos.json').read_text())

OUTPUT_SETTINGS = json.loads((ROOT/'objects'/'ffmpeg_settings.json').read_text())
FILE_TYPE = OUTPUT_SETTINGS["file_type"]
CODEC = OUTPUT_SETTINGS["codec"]

#APP = QApplication()

MEDIA_TYPE = {
    "Video": (".mp4", ".mov", ".mkv", ".avi"),
    "Image": (".png", ".jpeg", ".jpg", ".tiff", ".tga")
}
FRAMERATE = ["12.0", "23.98", "24.0", "$25.0", "29.97", "30.0", "50.0",
             "59.94", "60.0"]
