# -*- coding: utf-8 -*-
from viewer.ui.widgets.custom_widgets import Slider, ComboBox, SpinBox


def convert_type_to_widget(widget_name, widget_value, function=None):
    """create a widget from type value

    Arguments:
        widget_name {str} -- the widget attribute name
        widget_value -- value(s) the widget will take
    Keyword Arguments:
        function {method} method when value changed -- default: {None}

    Returns:
        [tuple] -- the widget name and QWidget object corresponding
            None if type is no recognized
    """

    if isinstance(widget_value, (list, tuple)):
        widget_name = f"cb_{widget_name}"
        widget = ComboBox(items=widget_value)
        if function:
            widget.currentTextChanged.connect(function)

    elif isinstance(widget_value, int):
        widget_name = f"s_{widget_name}"
        widget = Slider(widget_value, min=0, max=100, unit="%", precision=0)
        if function:
            widget.valueChanged.connect(function)

    elif isinstance(widget_value, float):
        widget_name = f"sb_{widget_name}"
        widget = SpinBox(widget_value, min=0, max=65536, precision=0)
        if function:
            widget.valueChanged.connect(function)

    else:
        return

        widget.setObjectName(widget_name.replace("_", " "))

    return (widget_name, widget)


def set_joined_widget_position(widget_s, index=None, max=None):
    """set position property to every widget from a list by it index

    Arguments:
        widget_s (list, str): a list or juste one widget
    Keyword Arguments:
        index {int} if juste one widget have to be set, give its index position
            -- default: {None}
        index {int} if juste one widget have to be set, the max position
            -- default: {None}
    """

    if index == 0:
        widget_s.setProperty("position", "up")
    elif index == max:
        widget_s.setProperty("position", "down")
    else:
        widget_s.setProperty("position", "middle")
