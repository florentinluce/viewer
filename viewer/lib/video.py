# -*- coding: utf-8 -*-

__author__ = "LUCE Florentin"


import cv2
import subprocess
import json
import re
import numpy as np
from pathlib import Path

from PySide2.QtCore import QThread
from PySide2.QtCore import Signal


def eval_attr(attribute):
    if not type(attribute) == str:
        return attribute

    # is an int
    if attribute.isdigit():
        return int(attribute)

    # is a float
    elif re.match(r'(\d+)\.(\d+)', attribute):
        return float(attribute)

    # is an operation
    elif re.match(r'(\d+)\/(\d+)', attribute):
        return eval(attribute)

    else:
        return str(attribute)


class Media:
    def __init__(self, _path):

        if Path(_path).is_dir():
            return

        self.path = Path(_path)
        self.name = self.path.name
        self.extension = self.path.suffix

        self.parse_media()

    def parse_media(self):

        cmd = ['ffprobe',
               '-v', 'quiet',
               '-print_format', 'json',
               '-show_format',
               '-show_streams',
               '-i',
               self.path]
        p = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out, err = p.communicate()

        self.data = json.loads(out)

        for stream in self.data['streams']:
            for k, v in stream.items():
                var = k
                if stream['index'] != 0:
                    var = "%s_%s" % (stream['index'], k)
                try:
                    setattr(self, var, eval_attr(v))
                except:
                    continue
                finally:
                    if var == "nb_frames":
                        self.first_frame = 1
                        self.last_frame = eval_attr(v)-1

        for k, v in self.data['format'].items():
            setattr(self, k, eval_attr(v))

    def open_video(self):
        self.buffer = []
        self.buffer_thread = Buffering(self)
        self.buffer_thread.start()
        self.buffer_thread.frame_read.connect(self.fill_buffer)

    def fill_buffer(self, frame):
        self.buffer.append(frame)


class Video(Media):
    def __init__(self, _path):
        super(Video, self).__init__(_path)


class ImagesSequence(Media):
    def __init__(self, _path):
        super(ImagesSequence, self).__init__(_path)

        regex = r"(\d+)%s$" % self.extension
        pattern = re.sub(regex, f"*{self.extension}", self.name)
        self.pattern_path = self.path.parent/pattern
        self.images_sequence = sorted(list(self.pattern_path.parent.glob(pattern)))

        start_frame = int(re.findall(regex, self.images_sequence[0].name)[0])
        last_frame = int(re.findall(regex, self.images_sequence[-1].name)[0])

        # fill missing frames
        if len(self.images_sequence) != last_frame - start_frame + 1:
            for nb in range(last_frame):
                # offset the frame by the starting one
                nb += start_frame
                frame = self.images_sequence[nb]
                frame_nb = int(re.findall(regex, frame.name)[0])

                if nb == frame_nb:
                    continue

                # if frame does not exist, create a blanck one
                self.images_sequence.insert(nb, None)

        self.nb_frames = len(self.images_sequence)
        self.first_frame = start_frame
        self.last_frame = last_frame


class Buffering(QThread):
    frame_read = Signal(np.ndarray)

    def __init__(self, media):
        super(Buffering, self).__init__()

        self.media = media
        self.is_running = False

    def run(self):
        self.is_running = True
        self.vc = cv2.VideoCapture(str(self.media.path))

        while self.is_running:
            if isinstance(self.media, Video):
                ret, frame = self.vc.read()
                if ret is True:
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    self.frame_read.emit(frame)
                else:
                    self.is_running = False

            elif isinstance(self.media, ImagesSequence):
                for image in self.media.images_sequence:
                    frame = cv2.imread(str(image))
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    self.frame_read.emit(frame)
                self.is_running = False

        self.vc.release()
