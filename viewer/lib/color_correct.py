# -*- coding: utf-8 -*-

import numpy as np


def adjust_gamma(gamma=1.0):
    """change image gamma with new_img = img^(1/gamma)

    Keyword Arguments:
        gamma {float} -- new gamma value (default: {1.0})
    Return:
        {np.ndarray} -- LUT table
    """

    if gamma == 0:
        gamma = 0.0000001
    inv_gamma = 1.0/gamma

    # Crée une table de LUT sur 256 valeurs
    table = np.array([((i / 255.0) ** inv_gamma) * 255
                      for i in np.arange(0, 256)]).astype("uint8")

    return table


def adjust_exposure(exposure=0.0):
    """change image exposure with new_img = img*2^exposure

    Keyword Arguments:
        gamma {float} -- new exposure value (default: {0.0})
    Return:
        {np.ndarray} -- LUT table
    """

    # Crée une table de LUT sur 256 valeurs
    table = np.array([min(((i / 255.0) * 2 ** exposure) * 255, 255)
                      for i in np.arange(0, 256)]).astype("uint8")

    return table
