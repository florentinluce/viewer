# -*- coding: utf-8 -*-
from viewer.lib.utils import convert_type_to_widget
from viewer.globals import CODEC


class Codec():
    def __init__(self, name, data):
        self.name = name
        self.command = CODEC[self.name]["command"]
        self.data = data

        self.used_attr = {}

    def set_ui(self):
        """create all widgets for this codec

        Returns:
            dict : a dict with used widgets their name and value
        """
        for n, (attr, value) in enumerate(self.data.items()):
            attr_name = attr.replace(" ", "_")
            widget = convert_type_to_widget(attr_name, value,
                                            self.on_value_changed)
            if not widget:
                continue

            self.used_attr[attr] = {}
            self.used_attr[attr]["widget_name"] = widget[0]
            self.used_attr[attr]["widget"] = widget[1]
            self.used_attr[attr]["value"] = widget[1].value()

        return self.used_attr

    def on_value_changed(self):
        """update value from widgets used"""

        for attr in self.data.keys():
            if attr == "command":
                continue
            value = self.used_attr[attr]["widget"].value()
            self.used_attr[attr]["value"] = value

    def get_pixel_format(self, chrominance, depth):
        if depth == "8 bits (35-235)" and chrominance != "gray":
            return chrominance
        elif depth == "8 bits (0-255)" and chrominance != "gray":
            return chrominance.replace("yuv", "yuvj")
        elif depth == "10 bits":
            return "%s10le" % chrominance
        else:
            return chrominance

    def to_dict(self):
        """get a dict with values set

        Returns:
            dict : all data from codec
        """

        codec_data = {attr: data["value"] for (attr, data) in
                      self.used_attr.items()}
        codec_data["name"] = self.name
        codec_data["command"] = self.command
        codec_data["pix_format"] = self.get_pixel_format(codec_data["chrominance"],
                                                         codec_data["color depth"])

        return codec_data
