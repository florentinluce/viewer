# -*- coding: utf-8 -*-

__author__ = "LUCE Florentin"

import cv2
import datetime
import numpy as np

from pathlib import Path
from PySide2.QtCore import Signal, QObject, QTimer, QThread

from viewer.objects.media import MEDIA_TYPE, Media, Video, Image
from viewer.objects.stamp import Stamp
from viewer.ui.player import PlayerView
from viewer.lib.color_correct import adjust_gamma, adjust_exposure


class Player(QObject):
    """Video player view to display frames"""

    frame_changed = Signal(int)
    media_changed = Signal(Media)
    time_unit_changed = Signal(str)
    framerate_changed = Signal(float)
    played = Signal()
    paused = Signal()
    stopped = Signal()

    stamps_updated = Signal()
    stamp_added = Signal(Stamp)

    def __init__(self):
        super(Player, self).__init__()

        self.player_view = PlayerView(self)

        self.media = None
        self.stamps = []
        self.gamma_table = adjust_gamma(1)
        self.expo_table = adjust_exposure(0)
        self.background_color = [0, 0, 0]

        self.is_playing = False
        self.time_unit = "seconds"
        self.looping = False
        self.current_frame = None
        self.framerate = 25
        self._current_frame_num = 0
        self.start_frame = 0
        self.end_frame = 100
        self.current_time = "00:00:00"
        self.end_time = "00:00:04"

        self.timer = QTimer()
        self.timer.timeout.connect(self.run)

        self.frame_changed.connect(self.on_frame_change)

    @property
    def current_frame_num(self):
        return self._current_frame_num

    @current_frame_num.setter
    def current_frame_num(self, value):
        self._current_frame_num = value
        self.frame_changed.emit(self._current_frame_num)

    def run(self):
        self.current_frame_num += 1

        self.current_time = str(datetime.timedelta(seconds=int(
            self.current_frame_num/self.framerate)))

    def toggle_play(self):
        if not self.media:
            return

        if self.is_playing:
            self.pause()
        else:
            self.play()

    def play(self):
        self.is_playing = True
        self.timer.start(1000/self.framerate)
        self.played.emit()

    def stop(self):
        self.is_playing = False
        self.timer.stop()
        self.stopped.emit()

    def pause(self):
        self.stop()
        self.paused.emit()

    def to_previous(self):
        self.current_frame_num -= 1

    def to_next(self):
        self.current_frame_num += 1

    def to_first(self):
        self.current_frame_num = self.start_frame

    def to_last(self):
        self.current_frame_num = self.end_frame

    def set_looping(self, value):
        self.looping = value

    def change_time_unit(self, time_unit):
        self.time_unit = time_unit
        self.time_unit_changed.emit(self.time_unit)
        self.frame_changed.emit(self.current_frame_num)

    def change_framerate(self, value):
        self.framerate = float(value)

        self.end_time = str(datetime.timedelta(seconds=int(
            self.end_frame/self.framerate)))
        self.current_time = str(datetime.timedelta(seconds=int(
            self.current_frame_num/self.framerate)))

        self.timer.setInterval(1000/self.framerate)

        self.framerate_changed.emit(self.framerate)

    def on_frame_change(self):

        # stop the video if last frame is reached
        if self.is_last_frame() and self.looping:
            self.to_first()
            self.play()
        elif self.is_last_frame():
            self.pause()

        self.current_frame = self.buffer[self.current_frame_num]

        # apply gamma LUT on frame
        adjust_frame = cv2.LUT(self.current_frame, self.gamma_table)
        adjust_frame = cv2.LUT(adjust_frame, self.expo_table)

        self.player_view.set_frame_in_player(adjust_frame)

    def is_last_frame(self):
        if self.current_frame_num == self.end_frame:
            return True
        return False

    def close_video(self):
        self.media = None

    def adjust_gamma(self, value):
        self.gamma_table = adjust_gamma(value)

    def adjust_exposure(self, value):
        self.expo_table = adjust_exposure(value)

    def set_media(self, path):

        extension = Path(path).suffix

        if extension in MEDIA_TYPE["Video"]:
            self.media = Video(path)
        elif extension in MEDIA_TYPE["Image"]:
            self.media = Image(path)
        else:
            return

        self.framerate = self.media.r_frame_rate
        self.start_frame = 0
        self.end_frame = self.media.end_frame
        self.end_time = str(datetime.timedelta(seconds=int(
            self.end_frame/self.framerate)))

        self.open_buffer()
        self.media_changed.emit(self.media)

    def open_buffer(self):
        self.buffer = []
        self.buffer_thread = Buffering(self.media)
        self.buffer_thread.start()
        self.buffer_thread.frame_read.connect(self.fill_buffer)

    def fill_buffer(self, frame):
        self.buffer.append(frame)

    def set_stamps(self, stamps_data):

        self.stamps = [Stamp.from_dict(data) for data in stamps_data]
        self.stamps_updated.emit()

    def add_stamp(self, data=None):
        stamp = Stamp()
        stamp.updated.connect(self.stamps_updated.emit)
        self.stamps.append(stamp)

        self.stamp_added.emit(stamp)

    def remove_stamp(self):
        pass


class Buffering(QThread):
    frame_read = Signal(np.ndarray)

    def __init__(self, media):
        super(Buffering, self).__init__()

        self.media = media
        self.is_running = False

    def run(self):
        self.is_running = True
        self.vc = cv2.VideoCapture(str(self.media.path))

        while self.is_running:
            if isinstance(self.media, Video):
                ret, frame = self.vc.read()
                if ret is True:
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    self.frame_read.emit(frame)
                else:
                    self.is_running = False

            elif isinstance(self.media, Image):
                for image in self.media.images_sequence:
                    frame = cv2.imread(str(image))
                    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    self.frame_read.emit(frame)
                self.is_running = False

        self.vc.release()
