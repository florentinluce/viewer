# -*- coding: utf-8 -*-

import ffmpeg
import json
from pathlib import Path
import argparse

from viewer.objects.media import MEDIA_TYPE, Video, Image, Audio

SETTINGS_PATH = Path(__file__).parent/"ffmpeg_settings.json"
SETTINGS = json.loads(SETTINGS_PATH.read_text())


class Converter(object):
    def __init__(self, input_path=None):

        self.settings = SETTINGS
        self.codecs = self.settings["codec"]

        self.input = input_path
        self._output = ""
        self._framerate = 25
        self._start_frame = 0
        self._end_frame = 100
        self.width = 0
        self.height = 0
        self._command = None
        self._pix_format = None

    @classmethod
    def from_dict(cls, input_path, data):
        converter = cls(input_path)
        converter.set_data(data)

        return converter

    def get_media(self, input):
        """analyse the input set to get correct media type

        Args:
            input (str, Path, Media): media path or Media object

        Returns:
            [Video, Image, Audio]: a Media object set as input
        """

        if not isinstance(input, (str, Path)):
            return input

        extension = Path(input).suffix
        if extension in MEDIA_TYPE["Video"]:
            return Video(input)
        if extension in MEDIA_TYPE["Image"]:
            return Image(input)
        if extension in MEDIA_TYPE["Audio"]:
            return Audio(input)

    def set_data(self, data):
        """convert dict data as attribute

        Args:
            data (dict): a dictionnary with all conversion data
        """

        self.start_frame = data["start_frame"]
        self.end_frame = data["end_frame"]
        self.framerate = data["framerate"]
        self.width = data["width"]
        self.height = data["height"]

        if (data["width"] != self.media.width or
           data["height"] != self.media.height):
            self.resize(data["width"], data["height"])

        if data["codec"]:
            self.set_codec(codec=data["codec"]["name"],
                           pix_format=data["codec"]["pix_format"])

        if data["watermark"]:
            self.add_overlay(image_path=data["watermark"]["path"],
                             adjust=data["watermark"]["adjust"])

        if data["output"]:
            self.output = data["output"]

    def set_range(self, start=None, end=None):
        if start:
            self.stream = self.stream.trim(start_frame=start)
        elif end:
            self.stream = self.stream.trim(end_frame=end)
        elif start and end:
            self.stream = self.stream.trim(start_frame=start, end_frame=end)

    @property
    def input(self):
        return self._input

    @input.setter
    def input(self, value):
        self._input = value
        self.media = self.get_media(self._input)
        self.stream = ffmpeg.input(self.media.path)

    @property
    def framerate(self):
        return self._framerate

    @framerate.setter
    def framerate(self, value):
        self._framerate = value
        factor_duration = self.media.r_frame_rate/self._framerate
        self.stream = ffmpeg.filter(self.stream,
                                    "setpts",
                                    "%s*PTS" % factor_duration)

    @property
    def start_frame(self):
        return self._start_frame

    @start_frame.setter
    def start_frame(self, value):
        self._start_frame = value
        self.set_range(start=self._start_frame)

    @property
    def end_frame(self):
        return self._end_frame

    @end_frame.setter
    def end_frame(self, value):
        self._end_frame = value
        self.set_range(end=self._end_frame)

    def resize(self, width, height):
        self.stream = self.stream.filter("scale", w=width, h=height)

    def set_codec(self, codec=None, pix_format=None):
        if codec not in self.codecs:
            print("codec is not recognized")
            return
        self._command = self.codecs[codec]["command"]
        self._pix_format = pix_format

    def add_overlay(self, image_path=None, adjust="None"):

        print(" ")
        print(adjust)

        overlay_stream = ffmpeg.input(image_path)
        overlay_media = Image(image_path)

        w, h = self.width, self.height
        x = 0.5 * (w - overlay_media.width)
        y = 0.5 * (h - overlay_media.height)

        video_ratio = w/h
        overlay_ratio = overlay_media.width/overlay_media.height

        if adjust == "Fit":
            if overlay_ratio > video_ratio:
                h = w*overlay_ratio
                x, y = 0, 0.5 * (self.height - h)
            else:
                w = h*overlay_ratio
                x, y = 0.5 * (self.width - w), 0

        elif adjust == "Crop":
            if overlay_ratio < video_ratio:
                h = w/overlay_ratio
                x, y = 0, 0.5 * (self.height - h)
            else:
                w = h/overlay_ratio
                x, y = 0.5 * (self.width - w), 0

        elif adjust == "Stretch":
            x, y = 0, 0

        if adjust != "None":
            overlay_stream = overlay_stream.filter("scale", w=w, h=h)

        self.stream = self.stream.overlay(overlay_stream, x=x, y=y)

    @property
    def output(self):
        return self._output

    @output.setter
    def output(self, value):
        self._output = Path(value)
        self.folder = self._output.parent
        self.name = self._output.stem

    def run(self):
        if not self.folder.exists():
            self.folder.mkdir()

        self.stream = self.stream.output(self.output.as_posix(),
                                         strict=-2,
                                         vcodec=self._command,
                                         pix_fmt=self._pix_format)
        ffmpeg.run(self.stream)


if __name__ == "__main__" :
    pass