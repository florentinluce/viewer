__author__ = "LUCE Florentin"

import json
from pathlib import Path

from PySide2.QtCore import QObject, Signal

from viewer.ui.widgets.stamp import StampGroupBox, StampItem

STAMP_INFOS_PATH = Path(__file__).parent/"stamp_infos.json"
STAMP_INFOS = json.loads(STAMP_INFOS_PATH.read_text())


class Stamp(QObject):
    """Stamp object with all tag information

    Keyword Arguments:
        name {string} -- stap name (default: {None})
    """

    updated = Signal()

    def __init__(self, name=None):
        super(Stamp, self).__init__()

        self.name = name
        # create and set the exportable attributs
        for attr, value in STAMP_INFOS["main_attributs"].items():
            setattr(self, attr, value)

        self.text_positions = STAMP_INFOS["text_positions"]
        self.usabled_expressions = STAMP_INFOS["expressions"]

        self.lwi_stamp = StampItem(self)
        self.gb_stamp = StampGroupBox(self)

        # CONNECTIONS

    @classmethod
    def from_dict(cls, data):
        """constructeur use by json import

        Arguments:
            data {dict} -- dict with all informations found
            in stamp_infos['main_attributs']
        Returns:
            [Stamp] -- the stamp created
        """

        stamp = cls(name=data['name'])

        for attr in STAMP_INFOS["main_attributs"]:
            setattr(stamp, attr, data[attr])

        return stamp

    def to_dict(self):
        """convert main attributs in dict

        Returns:
            [dict] -- all main attributs
        """

        stamp_dict = {}
        for attr in STAMP_INFOS["main_attributs"]:
            stamp_dict[attr] = getattr(self, attr)

        return stamp_dict

    def set_name(self, name):
        """set stamp name

        Arguments:
            name {string} -- [description]
        """

        self.name = name
        self.gb_stamp.setTitle(name)

    def remove(self):
        self.gb_stamp.setParent(None)
        self.lwi_stamp.remove()
