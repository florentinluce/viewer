# -*- coding: utf-8 -*-

import re
import ffmpeg

from pathlib import Path

MEDIA_TYPE = {
    "Video": (".mp4", ".mov", ".mkv", ".avi"),
    "Image": (".png", ".jpeg", ".jpg", ".tiff", ".tga", ".exr"),
    "Audio": (".mp3", ".wav", ".m4a")
}


def eval_attr(attribute):
    """convert an attribute into a correct type

    Args:
        attribute (str): a string to evaluate

    Returns:
        [int, str, float]: same value with a correct type
    """

    if not type(attribute) == str:
        return attribute

    # is an int
    if attribute.isdigit():
        return int(attribute)

    # is a float
    elif re.match(r'(\d+)\.(\d+)', attribute):
        return float(attribute)

    # is an operation
    elif re.match(r'(\d+)\/(\d+)', attribute):
        return eval(attribute)

    else:
        return str(attribute)


class Media():
    def __init__(self, _path):
        self.path = Path(_path)
        self.name = self.path.name
        self.extension = self.path.suffix

        self.parse_media()

    def parse_media(self):
        """Use ffprobe to get all media streams and formats informations"""

        self.data = ffmpeg.probe(self.path.as_posix())

        for stream in self.data['streams']:
            for k, v in stream.items():
                var = k
                if stream['index'] != 0:
                    var = "%s_%s" % (stream['index'], k)
                try:
                    setattr(self, var, eval_attr(v))
                except:
                    continue

        for k, v in self.data['format'].items():
            setattr(self, k, eval_attr(v))


class Image(Media):
    def __init__(self, _path):
        super(Image, self).__init__(_path)

        self.images_sequence = []

        # write a regex to find frame num and extension
        regex = re.compile(r"(\d+)%s$" % self.extension)
        # replace in frame name the regex by "*.ext"
        pattern = regex.sub(f"*{self.extension}", self.name)

        if "*" in pattern:
            self.find_all(pattern, regex)
        else:
            # image is just alone
            self.nb_frames = 1

    def find_all(self, pattern, regex):
        """Find all images from a pattern

        Args:
            pattern (string): the image sequence pattern
            regex (regex): the image base name without frame num and extension
        """

        self.pattern_path = self.path.parent/pattern
        self.images_sequence = sorted(list(self.pattern_path.parent.glob(pattern)))

        self.start_frame = int(regex.findall(self.images_sequence[0].name)[0])
        self.end_frame = int(regex.findall(self.images_sequence[-1].name)[0])

        # fill missing frames
        if len(self.images_sequence) != self.end_frame - self.start_frame + 1:
            for nb in range(self.start_frame, self.end_frame):
                frame = self.images_sequence[nb]
                frame_nb = int(regex.findall(frame.name)[0])

                if nb == frame_nb:
                    continue

                # if frame does not exist, create a blanck one
                self.images_sequence.insert(nb, None)

        self.nb_frames = self.end_frame - self.start_frame + 1
        self.ratio = self.width/self.height


class Video(Media):
    def __init__(self, _path):
        super(Video, self).__init__(_path)

        self.start_frame = 0
        self.end_frame = self.nb_frames - 1
        self.ratio = self.width/self.height


class Audio(Media):
    def __init__(self, _path):
        super(Audio, self).__init__(_path)
