import sys
from PySide2.QtWidgets import QApplication


if __name__ == "__main__":
    app = QApplication(sys.argv)
    app.setStyle("macintosh")

    from viewer.ui import main_window

    ui = main_window.Window()
    sys.exit(app.exec_())

    """from viewer.lib.ffmpeg_conversion import Converter, Image, Video

    input_path = "/Users/florentin/Movies/Infographie/Projets/BlueFlowers.mov"
    data = {'output':'/Users/florentin/Desktop/test.mov',
    'width': 960.0,
    'height': 540.0,
    'first_frame': 1.0,
    'last_frame': 50.0,
    'framerate': 25.0,
    'codec': {'chrominance': 'yuv444p', 'color depth': '10 bits', 'name': 'ProRes', 'command': 'prores_ks', 'pix_format': 'yuv444p10le'},
    'watermark': {'path': '/Users/florentin/Desktop/paperblue-net-forest-shack-bl.jpg', 'adjust': 'Crop'}
    }

    stream = Converter.from_dict(input_path, data)
    stream.run()"""